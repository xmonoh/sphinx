FROM gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest

MAINTAINER xmonoh@cern.ch

# Install pip and make
RUN /usr/bin/yum install -y python-pip make git doxygen

# Upgrade pip
RUN pip install --upgrade pip

# Install sphinx + extensions such as rtd theme, breathe
RUN pip install -U sphinx sphinx-argparse sphinx_rtd_theme m2r breathe

